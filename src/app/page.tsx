"use client";

import { useEffect, useState } from "react";
import { ProductCard } from "./components";

interface Product {
  id: string;
  name: string;
  image: string;
}

const Home = () => {
  const [products, setProducts] = useState<Product[]>([]);
  const [images, setImages] = useState<{ [key: string]: string }>({});

  useEffect(() => {
    const fetchProducts = async () => {
      const resProducts = await fetch(
        "https://www.giovankov.com/api/product.json"
      );
      const productsData = await resProducts.json();
      const resImages = await fetch("https://www.giovankov.com/api/image.json");
      const imagesData = await resImages.json();
      const imagesMap: { [key: string]: string } = {};

      imagesData.data.forEach((item: { id: string[]; image: string }) => {
        item.id.forEach((id: string) => {
          imagesMap[id] = item.image;
        });
      });

      setProducts(productsData.data);
      setImages(imagesMap);
    };

    fetchProducts();
  }, []);

  return (
    <div className="container mx-auto p-4">
      <section className="mb-8">
        <h1 className="text-4xl font-bold mb-4">Company Profile</h1>
        <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,
        molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum
        numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium
        optio, eaque rerum! Provident similique accusantium nemo autem. Veritatis
        obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam
        nihil, eveniet aliquid culpa officia aut! Impedit sit sunt quaerat, odit,
        tenetur error, harum nesciunt ipsum debitis quas aliquid. Reprehenderit,
        quia. Quo neque error repudiandae fuga? Ipsa laudantium molestias eos 
        sapiente officiis modi at sunt excepturi expedita sint? Sed quibusdam
        recusandae alias error harum maxime adipisci amet laborum. Perspiciatis 
        minima nesciunt dolorem! Officiis iure rerum voluptates a cumque velit 
        quibusdam sed amet tempora. Sit laborum ab, eius fugit doloribus tenetur 
        fugiat, temporibus enim commodi iusto libero magni deleniti quod quam 
        consequuntur! Commodi minima excepturi repudiandae velit hic maxime
        doloremque. Quaerat provident commodi consectetur veniam similique ad 
        earum omnis ipsum saepe, voluptas, hic voluptates pariatur est explicabo 
        fugiat, dolorum eligendi quam cupiditate excepturi mollitia maiores labore 
        suscipit quas? Nulla, placeat. Voluptatem quaerat non architecto ab laudantium
        modi minima sunt esse temporibus sint culpa, recusandae aliquam numquam 
        totam ratione voluptas quod exercitationem fuga. Possimus quis earum veniam 
        quasi aliquam eligendi, placeat qui corporis!
        </p>
      </section>
      <section>
        <h2 className="text-3xl font-bold mb-4">Available Products</h2>
        <div className="grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-4 gap-4">
          {products.map((product) => (
            <ProductCard
              key={product.id}
              id={product.id}
              name={product.name}
              image={images[product.id]}
            />
          ))}
        </div>
      </section>
    </div>
  );
};

export default Home;
