import React, { useState } from "react";

interface ProductCardProps {
  id: string;
  name: string;
  image: string;
}

const ProductCard: React.FC<ProductCardProps> = ({ id, name, image }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  return (
    <div className="border p-4 rounded-lg">
      {image ? (
        <img
          src={image}
          alt={name}
          className="w-full h-64 object-cover mb-4 cursor-pointer"
          onClick={() => setIsModalOpen(true)}
        />
      ) : (
        "Gambar tidak ada"
      )}
      <h3 className="text-2xl font-bold mb-2">{name}</h3>
      <p>{id}</p>
      {isModalOpen && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
          <div className="bg-white p-4 rounded">
            <img
              src={image}
              alt={name}
              className="w-full h-64 object-cover mb-4"
            />
            <button
              onClick={() => setIsModalOpen(false)}
              className="mt-4 bg-red-500 text-white px-4 py-2 rounded"
            >
              Close
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default ProductCard;
