import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';

interface Product {
  id: string;
  name: string;
  image: string;
}

const Home = () => {
  const [products, setProducts] = useState<Product[]>([]);
  const [images, setImages] = useState<{ [key: string]: string }>({});

  useEffect(() => {
    const fetchProducts = async () => {
      const resProducts = await fetch('https://www.giovankov.com/api/product.json');
      const productsData = await resProducts.json();
      const resImages = await fetch('https://www.giovankov.com/api/image.json');
      const imagesData = await resImages.json();
      const imagesMap = imagesData.reduce((acc: any, image: any) => {
        acc[image.id] = image.url;
        return acc;
      }, {});
      setProducts(productsData);
      setImages(imagesMap);
    };

    fetchProducts();
  }, []);

  return (
    <div className="container mx-auto p-4">
      <section className="mb-8">
        <h1 className="text-4xl font-bold mb-4">Company Profile</h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lacinia odio vitae vestibulum vestibulum.
        </p>
      </section>
      <section>
        <h2 className="text-3xl font-bold mb-4">Available Products</h2>
        <div className="grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-4 gap-4">
          {products.map((product) => (
            <ProductCard key={product.id} id={product.id} name={product.name} image={images[product.id]} />
          ))}
        </div>
      </section>
    </div>
  );
};

export default Home;
